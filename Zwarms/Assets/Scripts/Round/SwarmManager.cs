﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwarmManager : MonoBehaviour {
    private EnemySpawnController enemySpawnController;

    public static int MAX_INITIAL_ENEMY_AMOUNT = 20;
    public static float RESPAWNING_TIME = 3f; // time in seconds

    private float currentTime = 0f;
    
    private static int currentEnemiesAmount = 0;

    private bool isRunning = true;
    private bool isActivated = true;

    public bool IsRunning {
        get { return isRunning; }
        set { isRunning = value; }
    }

    private int currentMaxEnemyAmount;
    

    #region MonoBehaviour functions
    private void Start() {
        enemySpawnController = gameObject.GetComponent<EnemySpawnController>();
        currentMaxEnemyAmount = MAX_INITIAL_ENEMY_AMOUNT;
    }

    private void Update() {
        if ( CheckTimeToRespawn() && CheckEnemiesAmount() 
                && GameController.instance.EnemiesCanLive && isActivated ) {
            enemySpawnController.Spawn();
            currentEnemiesAmount++;

            ResetCurrentTime();
        }
    }
    #endregion

    #region Private functions
    private bool CheckTimeToRespawn () {
        currentTime -= Time.deltaTime;
        return currentTime <= 0f;
    }

    private void ResetCurrentTime() {
        currentTime = RESPAWNING_TIME;
    }

    private bool CheckEnemiesAmount () {
        return currentEnemiesAmount < currentMaxEnemyAmount;
    }
    #endregion


    #region Public functions
    public int GetCurrentEnemiesAmount () {
        return currentEnemiesAmount;
    }

    public void DecrementCurrentEnemiesAmount() {
        currentEnemiesAmount -= 1;
    }

    public void PreWarm () {
        for( int i = 0; i < currentMaxEnemyAmount;  i++ ) {
            enemySpawnController.Spawn();
        }
    }

    public void IncreaseDifficulty () {
        currentMaxEnemyAmount = (int) (currentMaxEnemyAmount * Constants.DIFFICULTY_FACTOR);
    }
    #endregion
}
