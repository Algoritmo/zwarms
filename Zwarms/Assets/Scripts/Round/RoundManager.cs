﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoundManager : MonoBehaviour {
    private static float ROUND_TIME = 90f; // time in seconds
    private static float BREAK_TIME = 15f;

    public float currentRoundTime;
    public float currentBreakTime;

    private SwarmManager swarmManager;

    private bool isActiveRound = true;
    private bool isOn = true;

    private int currentRound = 0;

    #region MonoBehaviour funtions
    private void Start() {
        swarmManager = gameObject.AddComponent<SwarmManager>();

        currentRoundTime = ROUND_TIME;
        currentBreakTime = BREAK_TIME;

        StartNewBreak();
    }

    private void Update() {
        if (isOn) {
            if (isActiveRound) {
                DecreaseRoundRemainingTime();

            } else {
                DecreaseWaitingTime();
            }
        }
    }
    #endregion


    #region Public functions
    public void DecrementEnemiesAmount() {
        swarmManager.DecrementCurrentEnemiesAmount();
    }

    public float GetCurrentTime () {
        return currentRoundTime < 0 ? 0f : currentRoundTime;
    }

    public bool WaitingForNextRound () {
        return currentRoundTime <= 0;
    }

    public int GetCurrentRoundTimeScore () {
        return (int) (ROUND_TIME - currentRoundTime);
    }
    #endregion


    #region Private functions
    private void DecreaseRoundRemainingTime () {
        currentRoundTime -= Time.deltaTime;

        if (currentRoundTime <= 0 ) {
            GameController.instance.IncreaseScore((int) ROUND_TIME);
            StartNewBreak();
        } 
    }

    private void StartNewBreak() {
        isActiveRound = false;
        swarmManager.IsRunning = false;
        GameController.instance.EnemiesCanLive = false;
        ResetWaitingTime();
        BGMManager.Instance.StopPlaying();

        GameController.instance.ActivateNextSwarmTimer((int)BREAK_TIME);
    }

    private void ResetWaitingTime() {
        currentBreakTime = BREAK_TIME;
    }


    private void DecreaseWaitingTime () {
        currentBreakTime -= Time.deltaTime;
        GameController.instance.UpdateNextSwarmTimer((int) currentBreakTime);

        if (currentBreakTime <= 0) {
            StartNewRound();
        }
    }

    private void StartNewRound() {
        currentRound++;

        isActiveRound = true;
        swarmManager.IsRunning = true;
        swarmManager.IncreaseDifficulty();

        GameController.instance.EnemiesCanLive = true;
        GameController.instance.FinishRound();
        GameController.instance.DeactivateNextSwarmTimer();
        GameController.instance.UpdateCurrentSwarmHUD(currentRound);

        ResetRoundTime();
        BGMManager.Instance.StartPlaying();
        swarmManager.PreWarm();

    }

    private void ResetRoundTime () {
        currentRoundTime = ROUND_TIME;
    }
    #endregion
}
