﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shaker : MonoBehaviour { 
    public float intensity = .1f;
    public float duration = .2f;

    private Transform target;
    private Vector3 initialPosition;

    private float pendingShakeDuration = 0f;

    private bool isShaking = false;

    #region MonoBehaviour functions
    private void Start() {
        target = gameObject.transform;
        initialPosition = target.position;
    }

    private void Update() {
        if ( pendingShakeDuration > 0   &&   !isShaking ) {
            StartCoroutine(DoShake());
        }
    }
    #endregion


    #region Public functions
    public void Shake() {
        if (duration > 0) {
            pendingShakeDuration += this.duration;
        }
    }

    public void Shake(float duration ) {
        if ( duration > 0 ) {
            pendingShakeDuration += duration;
        }
    }
    #endregion

    #region Coroutines
    IEnumerator DoShake () {
        isShaking = true;

        var startTime = Time.realtimeSinceStartup;
        Vector3 randomPoint;

        while (Time.realtimeSinceStartup < startTime + pendingShakeDuration ) {
            randomPoint = new Vector3(initialPosition.x + Random.Range(-1f, 1f) * intensity, initialPosition.y + Random.Range(-1f, 1f) * intensity, initialPosition.z);
            target.localPosition = randomPoint;

            yield return null;
        }


        pendingShakeDuration = 0;
        target.localPosition = initialPosition;
        isShaking = false;
    }

    #endregion
}
