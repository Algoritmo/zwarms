﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationManager : MonoBehaviour {
    private Animator animator;

    private void OnEnable() {
        if (!animator) {
            animator = GetComponentInChildren<Animator>();
        }
    }

    public virtual void StandBy() {

    }

    public virtual void Walk () {

    }

    public virtual void Run () {

    }

    public virtual void Death () {

    }

    public virtual void Attack() {

    }

    public virtual void Reload() {

    }

    public virtual void SwitchWeapon() {

    }

    public virtual void ResetAnimator() {

    }

    public virtual bool AnimationHasFinished() {
        return false;
    }

    public virtual bool AnimationHasJustStarted () {
        return false;
    }
}
