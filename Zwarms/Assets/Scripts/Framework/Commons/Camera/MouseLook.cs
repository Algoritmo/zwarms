﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour {
    public enum RotationAxes { MouseX, MouseY }
    public RotationAxes axes = RotationAxes.MouseY;

    private float currentSensitivityX = 1.5f;
    private float currentSensitivityY = 1.5f;

    private float sensitivityX = 1.5f;
    private float sensitivityY = 1.5f;

    private float rotationX;
    private float rotationY;

    private float minimumX = -360f;
    private float maximumX = 360f;

    private float minimumY = -60f;
    private float maximumY = 60f;

    private Quaternion originalRotation;

    private float mouseSensivity = 1.7f;


    void Start() {
        originalRotation = transform.rotation;
    }


    void Update() {
        HandleRotation();
        CursorControl();
    }


    void HandleRotation() {
        if (currentSensitivityX != mouseSensivity
            || currentSensitivityY != mouseSensivity) {
            currentSensitivityX = currentSensitivityY = mouseSensivity;
        }

        sensitivityX = currentSensitivityX;
        sensitivityY = currentSensitivityY;

        if (axes == RotationAxes.MouseX) {
            rotationX += Input.GetAxis("Mouse X") * sensitivityX;

            rotationX = ClampAngle(rotationX, minimumX, maximumX);
            Quaternion xQuaternion = Quaternion.AngleAxis(rotationX, Vector3.up);
            transform.localRotation = originalRotation * xQuaternion;
        }

        if (axes == RotationAxes.MouseY) {
            rotationY += Input.GetAxis("Mouse Y") * sensitivityY;

            rotationY = ClampAngle(rotationY, minimumY, maximumY);
            Quaternion yQuaternion = Quaternion.AngleAxis(-rotationY, Vector3.right);
            transform.localRotation = originalRotation * yQuaternion;
        }
    }


    private float ClampAngle(float angle, float min, float max) {
        if (angle < -360f) {
            angle += 360f;
        }

        if (angle > 360) {
            angle -= 360f;
        }

        return Mathf.Clamp(angle, min, max);
    }


    private void CursorControl() {
        if (Input.GetKeyDown(KeyCode.Tab)) {
            if (Cursor.lockState == CursorLockMode.Locked) {
                Cursor.lockState = CursorLockMode.None;
            } else {
                Cursor.lockState = CursorLockMode.Locked;
            }
        }
    }
}
