﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShakerController : MonoBehaviour {
    [SerializeField]
    private GameObject cameraObject;

    private Shaker camShaker;


    #region MonoBehaviour functions
    private void Start() {
        camShaker = cameraObject.GetComponent<Shaker>();
    }
    #endregion

    #region Public functions
    public void Shake(float intensity) {
        camShaker.intensity = intensity;
        camShaker.Shake();
    }

    public void Shake(float duration, float intensity) {
        camShaker.intensity = intensity;
        camShaker.Shake(duration);
    }
    #endregion

}
