﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectPoolManager : MonoBehaviour {
    public GameObject originalGameObject;

    private List<GameObject> gameObjects;


    void Start() {
        gameObjects = new List<GameObject>();
        CreateInstance(Vector3.zero, Quaternion.identity);
    }


    #region Public functions
    public void UseInstance( Vector3 position, Quaternion rotation ) {
        foreach ( GameObject currentInstance in gameObjects ) {
            if (currentInstance.activeSelf == false) {
                ResetInstance(currentInstance, position, rotation);
                return;
            }
        }

        CreateInstance(position, rotation);
    }

    public void UseInstance(Vector3 position ) {
        foreach (GameObject currentInstance in gameObjects) {
            if (currentInstance.activeSelf == false) {
                ResetInstance(currentInstance, position, Quaternion.identity);
                return;
            }
        }

        CreateInstance(position, Quaternion.identity);
    }

    public void SetOriginalGameObject ( GameObject newOriginalGameObject) {
        originalGameObject = newOriginalGameObject;
        CreateInstance(Vector3.zero, Quaternion.identity);
        originalGameObject.SetActive(false);
    }

    public void HideAll () {
        foreach ( GameObject instance in gameObjects ) {
            instance.SetActive(false);
        }
    }
    #endregion



    #region Private functions
    public virtual void ResetInstance ( GameObject instance, Vector3 position, Quaternion rotation ) {
        instance.transform.position = position;
        instance.transform.rotation = rotation;
        instance.SetActive(true);
        foreach ( MonoBehaviour component in instance.GetComponents(typeof(MonoBehaviour))) {
            component.enabled = true;
        }
    }

    private void CreateInstance ( Vector3 position, Quaternion rotation ) {
        if (originalGameObject) {
            GameObject auxObject = Instantiate(originalGameObject, position, rotation);
            auxObject.SetActive(true);
            auxObject.transform.SetAsFirstSibling();

            gameObjects.Add(auxObject);
            gameObjects[0].SetActive(false);
        }
    }

    private void CreateInstance(Vector3 position) {
        GameObject auxObject = Instantiate(originalGameObject, position, Quaternion.identity);
        auxObject.SetActive(true);
        auxObject.transform.SetAsFirstSibling();

        gameObjects.Add(auxObject);
        //gameObjects[0].SetActive(false);
    }
    #endregion
}
