﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextHUDPoolManager : MonoBehaviour {
    [SerializeField]
    private GameObject canvas;
    [SerializeField]
    private GameObject originalGameObject;


    private List<GameObject> instances;

    #region MonoBehaviour functions
    private void Start() {
        instances = new List<GameObject>();
        CreateInstance("def", Vector3.zero);
    }
    #endregion


    #region Public functions
    public void UseInstance(string newText, Vector3 newPosition) {
        foreach (GameObject currentInstance in instances) {
            if (currentInstance.activeSelf == false) {
                currentInstance.gameObject.GetComponentInChildren<Text>().text = newText;
                ResetInstance(currentInstance, newPosition);
                return;
            }
        }

        CreateInstance(newText, newPosition);
    }
    #endregion


    #region Private functions
    private void ResetInstance(GameObject instance, Vector3 newPosition) {
        instance.GetComponent<DamagePopUpManager>().SetPosition(newPosition);
        instance.SetActive(true);
        foreach (MonoBehaviour component in instance.GetComponents(typeof(MonoBehaviour))) {
            component.enabled = true;
        }
    }

    private void CreateInstance(string newText, Vector3 newScreenPosition) {
        GameObject auxObject = Instantiate(originalGameObject, newScreenPosition, Quaternion.identity);

        auxObject.GetComponent<DamagePopUpManager>().SetPosition(newScreenPosition);
        auxObject.SetActive(true);
        auxObject.transform.SetParent( canvas.transform, false );

        instances.Add(auxObject);
        instances[0].SetActive(false);
    }
    #endregion
}
