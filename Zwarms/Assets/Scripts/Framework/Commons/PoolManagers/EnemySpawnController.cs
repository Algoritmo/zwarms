﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnController : MonoBehaviour {
    public GameObject [] enemiesPrefabs;
    public Transform[] spawningPoints;

    private GameObjectPoolManager[] enemiesPoolManagers;

    public void Start () {
        InitializeGameObjectsPoolManagers();
    }

    private void InitializeGameObjectsPoolManagers() {
        enemiesPoolManagers = new GameObjectPoolManager[enemiesPrefabs.Length];

        // Initialize the original GameObject to instantiate for every GameObjectPoolManager
        // and the GameObjectPoolManager's themselves
        for ( int i = 0; i< enemiesPrefabs.Length; i++ ) {
            enemiesPoolManagers[i] = gameObject.AddComponent< GameObjectPoolManager>();
            enemiesPoolManagers[i].originalGameObject = enemiesPrefabs[i];
            enemiesPoolManagers[i].originalGameObject.SetActive(false);
        }
    }

    public void Spawn() {
        int spawningPoint = Random.Range(0, spawningPoints.Length);
        int randomIdex = Random.Range(0, enemiesPoolManagers.Length);

        enemiesPoolManagers[randomIdex].UseInstance(spawningPoints[spawningPoint].position,
            Quaternion.identity );
    }
}
