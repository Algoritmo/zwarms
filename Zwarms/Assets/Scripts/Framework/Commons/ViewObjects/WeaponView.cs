﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponView : MonoBehaviour {

    public virtual void Fire () {

    }

    public virtual void Enable () {
        gameObject.SetActive(true);
    }

    public virtual void Disable () {
        gameObject.SetActive(false);
    }
}
