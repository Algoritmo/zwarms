﻿using System.Collections;
using System.Collections.Generic;


public class WeaponData {
    private string NAME;
    public string Name {
        get { return NAME; }
    }

    private float RANGE;
    public float Range {
        get { return RANGE;  }
    }

    private int MAX_DAMAGE;

    #region Bullets variables
    public int currentMagazineBullets = 0;
    public int nonMagazineBullets = 0;

    public int MAX_TOTAL_BULLETS = 0;
    public int MAX_MAGAZINE_CAPACITY = 0;
    #endregion


    /**
     * CONSTRUCTOR
     **/
    public WeaponData( string name, float range, int maxDamage, int maxMagazineCapacity,
            int currentMagazineBulletsAmount, int nonMagazineBulletsAmount, int maxTotalBullets) {
        NAME = name;
        RANGE = range;
        MAX_DAMAGE = maxDamage;

        currentMagazineBullets = currentMagazineBulletsAmount;
        nonMagazineBullets = nonMagazineBulletsAmount;

        MAX_MAGAZINE_CAPACITY = maxMagazineCapacity;
        MAX_TOTAL_BULLETS = maxTotalBullets;
    }

    /**
     * METHODS
     **/
    public virtual int GetCalculatedDamage() {
        return MAX_DAMAGE;
    }
}
