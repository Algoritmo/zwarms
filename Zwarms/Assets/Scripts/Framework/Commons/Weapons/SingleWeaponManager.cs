﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleWeaponManager : MonoBehaviour {
    private WeaponView weaponView;
    private WeaponData _weaponData;
    public WeaponData weaponData {
        get { return _weaponData; }
        set { _weaponData = value; }
    }


    public virtual void Fire() {
    }

    public virtual void AddToMultipleWeaponManager() {
    }

    public void Reload () {
        if ( weaponData.currentMagazineBullets + weaponData.nonMagazineBullets
                    >= weaponData.MAX_MAGAZINE_CAPACITY ) {
            weaponData.nonMagazineBullets +=
                weaponData.currentMagazineBullets - weaponData.MAX_MAGAZINE_CAPACITY;
            weaponData.currentMagazineBullets = weaponData.MAX_MAGAZINE_CAPACITY;


        } else {
            weaponData.currentMagazineBullets += weaponData.nonMagazineBullets;
            weaponData.nonMagazineBullets = 0;
        }
    }

    public void Hide() {
        gameObject.SetActive(false);
    }

    public void Display() {
        gameObject.SetActive(true);
    }
}
