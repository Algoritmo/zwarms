﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultipleWeaponManager : MonoBehaviour {
    private static MultipleWeaponManager instance;

    private List<SingleWeaponManager> weapons;

    private void Awake() {
        // singleton
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);


        weapons = new List<SingleWeaponManager>();
    }

    public virtual void SwitchToPreviousWeapon () {
    }

    public void AddSingleWeaponManager ( SingleWeaponManager newWeapon) {
        weapons.Add(newWeapon);
    }
}
