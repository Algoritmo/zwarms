﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class BGMManager : MonoBehaviour {
    private static BGMManager instance;
    public static BGMManager Instance {
        get { return instance; }
    }


    [SerializeField]
    private AudioClip [] bgmTracks;
    [SerializeField]
    private AudioMixer audioMixer;

    private List<AudioSource> audioSources;

    private bool isPlaying = false;
    private int counter = 0;


    #region MonoBehavior functions
    private void Start() {
        if (instance == null) {
            instance = this;
        } else if (instance != this) {
            Destroy(gameObject);
        }

        audioSources = new List<AudioSource>();
        AudioSource tempAudioSource;

        foreach (AudioClip audioClip in bgmTracks ) {
            tempAudioSource =  gameObject.AddComponent<AudioSource>();
            tempAudioSource.outputAudioMixerGroup = audioMixer.FindMatchingGroups("Master")[0];
            tempAudioSource.clip = audioClip;
            audioSources.Add( tempAudioSource );
        }
    }
    #endregion

    #region Public functions
    public virtual void StartPlaying () {
        if ( !isPlaying  ) {
            if (counter == audioSources.Count-1)
                counter = 0;

            audioSources[counter].Play();

            counter++;
            isPlaying = true;
        }
    }

    public virtual void StopPlaying() {
        if ( isPlaying) {
            audioSources[counter - 1].Stop();
            isPlaying = false;
        }
    }

    public void Pause () {
        Debug.Log(counter);
        Debug.Log(GetCurrentIndex());
        audioSources[GetCurrentIndex()].Pause();
    }

    public void Resume () {
        audioSources[GetCurrentIndex()].UnPause();
    }
    #endregion

    #region Private functions
    private int GetCurrentIndex() {
        if (counter == 0)
            return audioSources.Count-1;
        else
            return counter-1;
    }
    #endregion
}
