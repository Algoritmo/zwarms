﻿using UnityEngine;
using UnityEngine.Audio;

public class WeaponSFXManager : MonoBehaviour {
    [SerializeField]
    private AudioClip attackSFX;
    [SerializeField]
    private AudioClip hitSFX;
    [SerializeField]
    private AudioClip reloadSFX;
    [SerializeField]
    private AudioClip emptyShotSFX;

    [SerializeField]
    private AudioMixer globalAudioMixer;

    private AudioSource attackAudioSource;
    private AudioSource hitAudioSource;
    private AudioSource reloadAudioSource;
    private AudioSource emptyShotAudioSource;

    #region MonoBehaviour functions
    private void Start() {
        if (attackSFX != null) {
            attackAudioSource = gameObject.AddComponent<AudioSource>();
            attackAudioSource.outputAudioMixerGroup = globalAudioMixer.FindMatchingGroups("Master")[0];
            attackAudioSource.playOnAwake = false;
            attackAudioSource.clip = attackSFX;
        }

        if (hitSFX != null) {
            hitAudioSource = gameObject.AddComponent<AudioSource>();
            hitAudioSource.outputAudioMixerGroup = globalAudioMixer.FindMatchingGroups("Master")[0];
            hitAudioSource.playOnAwake = false;
            hitAudioSource.clip = hitSFX;
        }

        if (reloadSFX != null) {
            reloadAudioSource = gameObject.AddComponent<AudioSource>();
            reloadAudioSource.outputAudioMixerGroup = globalAudioMixer.FindMatchingGroups("Master")[0];
            reloadAudioSource.playOnAwake = false;
            reloadAudioSource.clip = reloadSFX;
        }

        if (emptyShotSFX != null) {
            emptyShotAudioSource = gameObject.AddComponent<AudioSource>();
            emptyShotAudioSource.outputAudioMixerGroup = globalAudioMixer.FindMatchingGroups("Master")[0];
            emptyShotAudioSource.playOnAwake = false;
            emptyShotAudioSource.clip = emptyShotSFX;
        }
    }
    #endregion


    #region Public functions
    public void PlayAttackSound() {
        Debug.Log("playing attack audio source.");
        PlaySound(attackAudioSource);
    }

    public void PlayEmptySound() {
        PlaySound(emptyShotAudioSource);
    }

    public void PlayHitSound() {
        PlaySound(hitAudioSource);
    }

    public void PlayReloadSound() {
        PlaySound(reloadAudioSource);
    }
    #endregion


    #region Private functions
    private void PlaySound(AudioSource audioSource) {
        if (audioSource != null) {
            audioSource.Play();
        }
    }
    #endregion
}
