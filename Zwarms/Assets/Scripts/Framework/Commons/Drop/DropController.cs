﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropController : MonoBehaviour {
    private static int bulletsPercentageToIncrease = 10;

    private void Update() {
        if (Vector3.Distance(transform.position, GameController.instance.GetPlayerPosition()) <= 1) {
            GameController.instance.IncreaseAmmo( bulletsPercentageToIncrease );
            gameObject.SetActive(false);
        }
    }
}
