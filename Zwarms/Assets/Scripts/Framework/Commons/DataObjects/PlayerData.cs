﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData {
    public static int MAX_HP = 100;
    public int criticalHealth = 20;
    public int hp = MAX_HP;

    /**
     * Movement-related variables
     **/
    public float WALK_SPEED = 5f;
    public float RUN_SPEED = 10f;
    public float CROUCH_SPEED = 4f;
    public float JUMP_SPEED = 6.6f;
    public float GRAVITY = 20f;

    public int score = 0;

    public PlayerData () {
    }

    public PlayerData ( int maxHP, float walkSpeed, float runSpeed, float jumpSpeed, float gravityForce ) {
        MAX_HP = maxHP;
        WALK_SPEED = walkSpeed;
        RUN_SPEED = runSpeed;
        JUMP_SPEED = jumpSpeed;
        GRAVITY = gravityForce;
    }


    public void DealDamage ( int damage ) {
        hp -= damage;
    }

    public void Heal ( int healingAmount ) {
        hp += healingAmount;

        if ( hp > MAX_HP )
            hp = MAX_HP;
    }

    public void Heal(float healingAmount) {
        hp += (int)healingAmount;

        if (hp > MAX_HP)
            hp = MAX_HP;
    }

    public bool IsAlive () {
        return hp > 0;
    }

    public void Reset () {
        hp = MAX_HP;
    }
}
