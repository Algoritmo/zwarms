﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constants {
    public static float DIFFICULTY_FACTOR = 1.2f;
    public static int SCORES_AMOUNT = 10;

    #region Key Binding
    public static string FORWARD = "forward";
    public static string BACKWARD = "backward";
    public static string LEFT = "left";
    public static string RIGHT = "right";
    public static string JUMP = "jump";

    public static string RELOAD = "reload";
    public static string PREVIOUS_WEAPON = "previousWeapon";

    public static string PAUSE = "pause";

    public static string WEAPON1 = "weapon1";
    public static string WEAPON2 = "weapon2";
    public static string WEAPON3 = "weapon3";
    public static string WEAPON4 = "weapon4";
    public static string WEAPON5 = "weapon5";
    public static string WEAPON6 = "weapon6";
    public static string WEAPON7 = "weapon7";
    public static string WEAPON8 = "weapon8";
    public static string WEAPON9 = "weapon9";

    public static string FIRE_MOUSE = "fireMouse";
    #endregion


    #region Other binding
    public static string GLOBAL_VOLUME = "volume";
    public static string HIGHSCORE = "highScore";
    public static string CURRENT_SCORE = "currentScore";

    public static string MAIN_LEVEL = "TestLevel";
    public static string MAIN_MENU = "MainMenu";
    #endregion


    #region Default key values
    public static KeyCode FORWARD_DEFAULT_KEY = KeyCode.W;
    public static KeyCode BACKWARD_DEFAULT_KEY = KeyCode.S;
    public static KeyCode LEFT_DEFAULT_KEY = KeyCode.A;
    public static KeyCode RIGHT_DEFAULT_KEY = KeyCode.D;

    public static KeyCode JUMP_DEFAULT_KEY = KeyCode.Space;
    public static KeyCode PREVIOUS_WEAPON_DEFAULT_KEY = KeyCode.Q;
    public static KeyCode FIRE_DEFAULT_KEY = KeyCode.Mouse0;
    public static KeyCode RELOAD_DEFAULT_KEY = KeyCode.R;

    public static KeyCode WEAPON1_DEFAULT_KEY = KeyCode.Alpha1;
    public static KeyCode WEAPON2_DEFAULT_KEY = KeyCode.Alpha2;
    public static KeyCode WEAPON3_DEFAULT_KEY = KeyCode.Alpha3;
    public static KeyCode WEAPON4_DEFAULT_KEY = KeyCode.Alpha4;
    public static KeyCode WEAPON5_DEFAULT_KEY = KeyCode.Alpha5;
    public static KeyCode WEAPON6_DEFAULT_KEY = KeyCode.Alpha6;
    public static KeyCode WEAPON7_DEFAULT_KEY = KeyCode.Alpha7;
    public static KeyCode WEAPON8_DEFAULT_KEY = KeyCode.Alpha8;
    public static KeyCode WEAPON9_DEFAULT_KEY = KeyCode.Alpha9;
    #endregion
}
