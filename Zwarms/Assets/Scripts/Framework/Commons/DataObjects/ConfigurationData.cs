﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfigurationData {
    public Dictionary<string, KeyCode> keyCodesBinding;
    public float globalVolume;
    public float mouseSensitivity;

    public string quality;
    public string resolution;
    public bool fullscreen;

    public ConfigurationData () {
        InitializeKeyConfiguration();

        globalVolume = PlayerPrefs.GetFloat(Constants.GLOBAL_VOLUME, 1);

        PlayerPrefs.Save();
    }

    

    #region Public functions
    public string GetKeyString( string keyBinding ) {
        return keyCodesBinding[keyBinding].ToString();
    }

    public void SetNewKey ( string keyBinding, KeyCode newKeyCode ) {
        keyCodesBinding[keyBinding] = newKeyCode;
        PlayerPrefs.SetString(keyBinding, newKeyCode.ToString());
    }

    public void Reset () {
        SetNewKey(Constants.FORWARD, Constants.FORWARD_DEFAULT_KEY);
        SetNewKey(Constants.BACKWARD, Constants.BACKWARD_DEFAULT_KEY);
        SetNewKey(Constants.LEFT, Constants.LEFT_DEFAULT_KEY);
        SetNewKey(Constants.RIGHT, Constants.RIGHT_DEFAULT_KEY);

        SetNewKey(Constants.PREVIOUS_WEAPON, Constants.PREVIOUS_WEAPON_DEFAULT_KEY);
        SetNewKey(Constants.RELOAD, Constants.RELOAD_DEFAULT_KEY);
        SetNewKey(Constants.FIRE_MOUSE, Constants.FIRE_DEFAULT_KEY);
        SetNewKey(Constants.JUMP, Constants.JUMP_DEFAULT_KEY);
    }

    public void SetVolume( float newVolumeValue ) {
        globalVolume = newVolumeValue;
        PlayerPrefs.SetFloat(Constants.GLOBAL_VOLUME, newVolumeValue);
    }

    public float GetVolume () {
        return globalVolume;
    }
    #endregion


    #region Private functions
    private void InitializeKeyConfiguration () {
        keyCodesBinding = new Dictionary<string, KeyCode>();

        /*
         * Tries to get the already player key configuration.
         * If there aren't saved configurations, sets default ones.
        */
        // Key Initialization
        keyCodesBinding.Add(Constants.FORWARD,
                    (KeyCode)System.Enum.Parse(
                    typeof(KeyCode), PlayerPrefs.GetString(Constants.FORWARD,
                    Constants.FORWARD_DEFAULT_KEY.ToString())));

        keyCodesBinding.Add(Constants.BACKWARD,
                    (KeyCode)System.Enum.Parse(
                    typeof(KeyCode), PlayerPrefs.GetString(Constants.BACKWARD,
                    Constants.BACKWARD_DEFAULT_KEY.ToString())));

        keyCodesBinding.Add(Constants.LEFT,
                    (KeyCode)System.Enum.Parse(
                    typeof(KeyCode), PlayerPrefs.GetString(Constants.LEFT,
                    Constants.LEFT_DEFAULT_KEY.ToString())));

        keyCodesBinding.Add(Constants.RIGHT,
                    (KeyCode)System.Enum.Parse(
                    typeof(KeyCode), PlayerPrefs.GetString(Constants.RIGHT,
                    Constants.RIGHT_DEFAULT_KEY.ToString())));

        keyCodesBinding.Add(Constants.JUMP,
                    (KeyCode)System.Enum.Parse(
                    typeof(KeyCode), PlayerPrefs.GetString(Constants.JUMP,
                    Constants.JUMP_DEFAULT_KEY.ToString())));

        keyCodesBinding.Add(Constants.PREVIOUS_WEAPON,
                    (KeyCode)System.Enum.Parse(
                    typeof(KeyCode), PlayerPrefs.GetString(Constants.PREVIOUS_WEAPON,
                    Constants.PREVIOUS_WEAPON_DEFAULT_KEY.ToString())));

        keyCodesBinding.Add(Constants.RELOAD,
                    (KeyCode)System.Enum.Parse(
                    typeof(KeyCode), PlayerPrefs.GetString(Constants.RELOAD,
                    Constants.RELOAD_DEFAULT_KEY.ToString())));

        keyCodesBinding.Add(Constants.FIRE_MOUSE,
                    (KeyCode)System.Enum.Parse(
                    typeof(KeyCode), PlayerPrefs.GetString(Constants.FIRE_MOUSE,
                    Constants.FIRE_DEFAULT_KEY.ToString())));
        // end Key Initialization
    }
    #endregion
}
