﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WASDMovementManager : MonoBehaviour {
    private Vector3 firstPersonViewRotation = Vector3.zero;
    private Vector3 moveDirection = Vector3.zero;
    private Vector3 defaultCamPosition;

    private CharacterController characterController;
    private Transform firstPersonView;


    private KeyCode moveForwardKey;
    private KeyCode moveBackwardKey;
    private KeyCode moveLeftKey;
    private KeyCode moveRightKey;
    private KeyCode jumpKey;


    private float inputX;
    private float inputY;
    private float inputXSet;
    private float inputYSet;
    private float camHeight;
    private float inputModifyFactor;
    private float rayDistance;
    private float defaultControllerHeight;
    private float antiBumpFactor = .75f;

    [SerializeField]
    private float walkSpeed = 7f;
    [SerializeField]
    private float gravity = 50f;
    [SerializeField]
    private float jumpSpeed;

    [SerializeField]
    private LayerMask groundLayer;


    private bool isMoving;
    private bool isGrounded;
    private bool limitDiagonalSpeed = true;
    private bool isCrouching = false;


    #region MonoBehaviour functions
    private void Start() {
        firstPersonView = transform;
        characterController = GetComponent<CharacterController>();
        isMoving = false;

        rayDistance = characterController.height * .5f + characterController.radius;
        defaultControllerHeight = characterController.height;

        InitializeKeys();
    }

    private void Update() {
        ApplyMovement();
    }
    #endregion


    #region Private functions
    private void InitializeKeys () {
        jumpKey = (KeyCode)System.Enum.Parse(
                    typeof(KeyCode), PlayerPrefs.GetString(Constants.JUMP,
                    Constants.JUMP_DEFAULT_KEY.ToString()));

        moveForwardKey = (KeyCode)System.Enum.Parse(
                    typeof(KeyCode), PlayerPrefs.GetString(Constants.FORWARD,
                    Constants.FORWARD_DEFAULT_KEY.ToString()));

        moveBackwardKey = (KeyCode)System.Enum.Parse(
                    typeof(KeyCode), PlayerPrefs.GetString(Constants.BACKWARD,
                    Constants.BACKWARD_DEFAULT_KEY.ToString()));

        moveLeftKey = (KeyCode)System.Enum.Parse(
                    typeof(KeyCode), PlayerPrefs.GetString(Constants.LEFT,
                    Constants.LEFT_DEFAULT_KEY.ToString()));

        moveRightKey = (KeyCode)System.Enum.Parse(
                    typeof(KeyCode), PlayerPrefs.GetString(Constants.RIGHT,
                    Constants.RIGHT_DEFAULT_KEY.ToString()));
    }

    private void ApplyMovement() {
        if (Input.GetKey(moveForwardKey) || Input.GetKey(moveBackwardKey)) {
            if (Input.GetKey(moveForwardKey)) {
                inputYSet = 1f;
            } else {
                inputYSet = -1f;
            }

        } else {
            inputYSet = 0f;
        }

        if (Input.GetKey(moveLeftKey) || Input.GetKey(moveRightKey)) {
            if (Input.GetKey(moveLeftKey)) {
                inputXSet = -1f;
            } else {
                inputXSet = 1f;
            }

        } else {
            inputXSet = 0f;
        }

        inputY = Mathf.Lerp(inputY, inputYSet, Time.deltaTime * 19f);
        inputX = Mathf.Lerp(inputX, inputXSet, Time.deltaTime * 19f);

        inputModifyFactor = Mathf.Lerp(inputModifyFactor,
            (inputYSet != 0 && inputXSet != 0 && limitDiagonalSpeed) ? .75f : 1f,
            Time.deltaTime * 19f);

        firstPersonViewRotation = Vector3.Lerp(firstPersonViewRotation, Vector3.zero, Time.deltaTime * 5f);
        //firstPersonView.localEulerAngles = firstPersonViewRotation;

        if (isGrounded) {
            moveDirection = new Vector3(inputX * inputModifyFactor, -antiBumpFactor, inputY * inputModifyFactor);
            moveDirection = transform.TransformDirection(moveDirection) * walkSpeed;

            Jump();
        }

        moveDirection.y -= gravity * Time.deltaTime;

        isGrounded = (characterController.Move(moveDirection * Time.deltaTime) & CollisionFlags.Below) != 0;
        isMoving = characterController.velocity.magnitude > .15f;
    }

    private void Jump() {
        if (Input.GetKeyDown(jumpKey)) {
            if (isCrouching) {
                if (CanGetUp()) {
                    isCrouching = false;

                    StopCoroutine(MoveCameraCrouch());
                    StartCoroutine(MoveCameraCrouch());
                }

            } else {
                moveDirection.y = jumpSpeed;
            }
        }
    }

    private bool CanGetUp() {
        Ray groundRay = new Ray(transform.position, transform.up);
        RaycastHit groundHit;

        if (Physics.SphereCast(
                groundRay,
                characterController.radius + 0.05f,
                out groundHit,
                rayDistance, groundLayer)) {
            if (Vector3.Distance(transform.position, groundHit.point) < 2.3f) {
                return false;
            }
        }

        return true;
    }

    IEnumerator MoveCameraCrouch() {
        characterController.height = isCrouching ? defaultControllerHeight / 1.5f : defaultControllerHeight;
        characterController.center = new Vector3(0f, characterController.height / 2f, 0f);

        camHeight = isCrouching ? defaultCamPosition.y / 1.5f : defaultCamPosition.y;

        while (Mathf.Abs(camHeight - firstPersonView.localPosition.y) > .01f) {
            firstPersonView.localPosition = Vector3.Lerp(
                firstPersonView.localPosition,
                new Vector3(defaultCamPosition.x, camHeight, defaultCamPosition.z),
                Time.deltaTime * 11f);
            yield return null;
        }
    }
    #endregion
}
