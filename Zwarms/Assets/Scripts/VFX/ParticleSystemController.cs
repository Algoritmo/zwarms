﻿using UnityEngine;

public class ParticleSystemController : MonoBehaviour {
    [SerializeField]
    private ParticleSystem particleSystem;

    #region MonoBehaviour functions
    private void Update() {
        if ( !particleSystem.IsAlive() ) {
            particleSystem.gameObject.SetActive(false);
        }
    }
    #endregion

}
