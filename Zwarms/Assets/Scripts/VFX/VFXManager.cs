﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VFXManager : MonoBehaviour {
    private static VFXManager instance;
    public static VFXManager Instance {
        get { return instance; }
    }


    [SerializeField]
    private GameObject bloodSplashVFX;


    #region Managers
    private GameObjectPoolManager bloodObjectPoolManager;
    #endregion


    #region MonoBehaviour
    private void Start() {
        if ( instance == null ) {
            instance = this;
        } else {
            Destroy(this.gameObject);
        }

        bloodObjectPoolManager = bloodSplashVFX.GetComponent<GameObjectPoolManager>();
    }
    #endregion


    #region Public functions
    public void SpawnBloodSplash ( Vector3 spawnPosition ) {
        bloodObjectPoolManager.UseInstance(spawnPosition);//, Quaternion.LookRotation(GameController.instance.GetPlayerPosition()));
    }
    #endregion
}
