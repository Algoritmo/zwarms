﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSystemPoolManager : GameObjectPoolManager {

    public override void ResetInstance(GameObject instance, Vector3 position, Quaternion rotation) {
        base.ResetInstance(instance, position, rotation);

        instance.transform.position = position;
        instance.transform.rotation = rotation;
        instance.SetActive(true);
        foreach (MonoBehaviour component in instance.GetComponents(typeof(MonoBehaviour))) {
            component.enabled = true;
        }

        instance.GetComponent<ParticleSystem>().Play();
    }
}
