﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BleedingScreenController : MonoBehaviour {
    public static BleedingScreenController Instance;

    private bool isActivated;
    private bool isInitialized;
    private bool isIncreasing = true;

    Color tempColour;

    private float MINIMUM_ALPHA = 0.4f;
    private float MAXIMUM_ALPHA = 0.85f;

    private static float incrementValue = 0.02f;
    private static float decrementValue = -0.01f;
    private float currentValue = incrementValue;

    private Image bleedingImage;

    #region MonoBehaviour functions
    private void Start() {
        isActivated = false;

        bleedingImage = gameObject.GetComponent<Image>();
        
        Instance = this;
    }

    private void Update() {
        PlayAnimation();
    }
    #endregion

    #region Private functions
    private void PlayAnimation () {
        if ( isActivated ) {
            tempColour = bleedingImage.color;

            if ( tempColour.a > MAXIMUM_ALPHA ) {
                currentValue = decrementValue;

            } else if ( tempColour.a < MINIMUM_ALPHA ) {
                currentValue = incrementValue;
            }
   
            tempColour.a += currentValue;
            bleedingImage.color = tempColour;
        }
    }
    #endregion


    #region Public functions
    public void Activate() {
        tempColour = bleedingImage.color;
        tempColour.a = MINIMUM_ALPHA;
        bleedingImage.color = tempColour;

        isActivated = true;
    }


    public void Deactivate() {
        tempColour = bleedingImage.color;
        tempColour.a = 0;
        bleedingImage.color = tempColour;

        isActivated = false;
    }
    #endregion
}
