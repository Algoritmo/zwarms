﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverTransitionController : MonoBehaviour {
    private static GameOverTransitionController instance;
    public static GameOverTransitionController Instance {
        get { return instance; }
    }

    private Image gameOverTransition;

    private bool startTransition = false;
    private Color tempColor;
    private float incrementalValue = 0.01f;


    #region MonoBehaviour functions
    private void Start () {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);

        gameOverTransition = gameObject.GetComponent<Image>();
    }

    private void Update() {
        if ( startTransition ) {
            tempColor = gameOverTransition.color;
            tempColor.a += incrementalValue;
            gameOverTransition.color = tempColor;

            if (tempColor.a > .9f) {
                SceneManager.LoadScene("GameOver");
            }

        }
    }
    #endregion

    #region Public functions
    public void StartTransition () {
        startTransition = true;
    }
    #endregion
}
