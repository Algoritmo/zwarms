﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour {
    #region HUDs controllers
    private LifeBarController lifeBarController;
    private TimerDisplayerController timerDisplayerController;
    private HighScoreController highScoreController;
    private AmmunitionController ammoController;
    private DamageHUDController damageHUDController;
    private NextSwarmTimerController nextSwarmTimerController;
    private CurrentRoundHUDController currentRoundDisplayerHUD;
    #endregion

    #region Menus controllers
    private PauseMenuController pauseMenuController;
    #endregion

    private GameObject settingsMenuObject;


    #region MonoBehaviour functions
    private void Start() {
        ammoController = gameObject.AddComponent<AmmunitionController>();
        lifeBarController = gameObject.AddComponent<LifeBarController>();
        timerDisplayerController = gameObject.AddComponent<TimerDisplayerController>();
        highScoreController = gameObject.AddComponent<HighScoreController>();
        pauseMenuController = gameObject.AddComponent<PauseMenuController>();
        
        damageHUDController = GameObject.Find("HUDs").GetComponent<DamageHUDController>();
        nextSwarmTimerController = GameObject.Find("HUDs").GetComponent<NextSwarmTimerController>();
        currentRoundDisplayerHUD = GameObject.Find("CurrentRound").GetComponent<CurrentRoundHUDController>();

        nextSwarmTimerController.Deactivate();

        settingsMenuObject = GameObject.Find("Settings");
    }
    #endregion


    #region Public functions
    public void UpdateTimer( float normalizedTime ) {
        timerDisplayerController.UpdateTimer( normalizedTime );
    }

    public void UpdateLifeBarWithDamage( float normalizedDamage ) {
        lifeBarController.DecrementLifeBar(normalizedDamage);
    }

    public void UpdateLifeBar(float normalizedHealth) {
        lifeBarController.SetHealthAmount(normalizedHealth);
    }

    public void UpdateHighScore( int highScore ) {
        highScoreController.UpdateCurrentScore(highScore);
    }

    public void UpdateAmmunition( int magazineBulletsAmount, int totalBulletsAmount ) {
        if ( ammoController )
            ammoController.UpdateAmmo(magazineBulletsAmount, totalBulletsAmount);
    }

    public void OpenPauseMenu () {
        pauseMenuController.OpenMenu();
    }

    public void ClosePauseMenu () {
        pauseMenuController.CloseMenu();
        settingsMenuObject.SetActive(false);
    }

    public void AddDamageHud( Vector3 hudPosition, Quaternion hudRotation, int damage) {
        damageHUDController.SpawnDamageHUD(hudPosition, hudRotation, damage);
    }

    public void UpdateSwarmHUD ( int swarm ) {
        currentRoundDisplayerHUD.UpdateRoundDisplayer(swarm);
    }


    #region NextSwarmTimer functions
    public void ActivateNextSwarmTimer (int timeInSeconds ) {
        UpdateNextSwarmTimer(timeInSeconds);
        nextSwarmTimerController.Activate();
    }

    public void UpdateNextSwarmTimer( int timeInSeconds) {
        nextSwarmTimerController.DisplayRemainingTime(timeInSeconds);
    }

    public void DeactivateNextSwarmTimer() {
        nextSwarmTimerController.Deactivate();
    }
    #endregion

    #endregion
}
