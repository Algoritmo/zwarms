﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerDisplayerController : MonoBehaviour {
    private Text timerDisplayer;
    private int temp, minutes, seconds;

    #region MonoBehaviour functions
    private void Start() {
        timerDisplayer = GameObject.Find("LeftTime").GetComponent<Text>();
    }


    #endregion

    #region Public functions
    public void UpdateTimer (float normalizedTime ) {

        timerDisplayer.text = FormatTime(normalizedTime) ;
    }
    #endregion


    #region Private functions
    private string FormatTime( float time ) {
        temp = (int) time;
        minutes = temp / 60;
        seconds = temp % 60;

        return (minutes < 10 ? "0" + minutes : "" + minutes) 
            + ":"
            + (seconds < 10 ? "0" + seconds : "" + seconds);
    }
    #endregion
}
