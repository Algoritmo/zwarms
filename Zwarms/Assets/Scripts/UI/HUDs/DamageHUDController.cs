﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DamageHUDController : MonoBehaviour {
    private TextHUDPoolManager textHUDPoolManager;
    

    #region MonoBehaviour functions
    private void Start () {
        textHUDPoolManager = gameObject.GetComponent<TextHUDPoolManager>();
    }
    #endregion


    public void SpawnDamageHUD ( Vector3 hudPosition, Quaternion hudRotation, int damage ) {
        textHUDPoolManager.UseInstance(damage.ToString(), hudPosition );
    }
}
