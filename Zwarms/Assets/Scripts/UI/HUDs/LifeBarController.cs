﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeBarController : MonoBehaviour {
    private Image lifeBarImage;
    private Text lifePercentage;

    private float temp;

    #region MonoBehaviour functions
    private void Start() {
        lifeBarImage = GameObject.Find("MainLifeBar").GetComponent<Image>();
        lifePercentage = GameObject.Find("LifePercentage").GetComponent<Text>();
    }
    #endregion

    #region Public functions
    public void SetHealthAmount ( float normalizedHealth) {
        lifeBarImage.fillAmount = normalizedHealth;
        lifePercentage.text = lifeBarImage.fillAmount * 100 + "%";
    }

    public void DecrementLifeBar( float normalizedDamage ) {
        temp = lifeBarImage.fillAmount;
        temp -= normalizedDamage;

        if (temp < 0f) 
            temp = 0;

        lifeBarImage.fillAmount = temp;
        lifePercentage.text = lifeBarImage.fillAmount * 100 + "%";
    }
    #endregion
}
