﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AmmunitionController : MonoBehaviour {
    private Text ammo;
    string tmp;

    private void Start() {
        ammo = GameObject.Find("AmmunitionDisplayer").GetComponent<Text>();
    }

    public void UpdateAmmo( int magazineBulletsAmount, int totalBulletsAmount ) {
        if (totalBulletsAmount == 0) {
            tmp = "--/--";
        } else if (magazineBulletsAmount == 0) {
            tmp = "00/" + totalBulletsAmount;
        } else {
            tmp = (magazineBulletsAmount < 10 ? "0" + magazineBulletsAmount : magazineBulletsAmount + "")
                + "/"
                + (totalBulletsAmount < 10 ? "0" + totalBulletsAmount : totalBulletsAmount + "");
        }
        if ( ammo != null )
            ammo.text = tmp;
    }
}
