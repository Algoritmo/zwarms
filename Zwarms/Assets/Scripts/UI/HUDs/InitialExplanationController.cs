﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitialExplanationController : MonoBehaviour {
    private void Start() {
        Invoke("AutoHide", 9f);
    }


    private void AutoHide () {
        this.gameObject.SetActive(false);
    }
}
