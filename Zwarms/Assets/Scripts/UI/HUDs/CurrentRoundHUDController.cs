﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CurrentRoundHUDController : MonoBehaviour {
    private Text roundText;

    #region MonoBehaviour functions
    private void Start() {
        roundText = gameObject.GetComponent<Text>();
    }
    #endregion

    #region Public functions
    public void UpdateRoundDisplayer( int round ) {
        roundText.text = "Swarm " + round;
    }
    #endregion
}
