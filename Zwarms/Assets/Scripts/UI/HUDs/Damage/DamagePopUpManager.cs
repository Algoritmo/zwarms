﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamagePopUpManager : MonoBehaviour {
    private Vector3 worldPosition = Vector3.zero;

    #region MonoBehaviour functions
    private void Update() {
        gameObject.transform.position = Camera.main.WorldToScreenPoint(worldPosition);
    }
    #endregion

    private void Disable() {
        transform.parent.gameObject.SetActive(false);
    }

    #region Public functions
    public void SetPosition ( Vector3 newWorldPosition ) {
        worldPosition = newWorldPosition;
    }
    #endregion
}
