﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextParentManager : MonoBehaviour {
    private void OnEnable() {
        GetComponentInChildren<DamagePopUpManager>().enabled = true;
    }

    private void Start() {
        transform.parent = GameObject.Find("HUDs").transform;
    }
}
