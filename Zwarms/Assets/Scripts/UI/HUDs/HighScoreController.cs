﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class HighScoreController : MonoBehaviour {
    private Text currentScore;

    private void Start() {
        currentScore = GameObject.Find("CurrentScore").GetComponent<Text>();
    }


    public void UpdateCurrentScore( int score ) {
        currentScore.text = score + "";
    }
}
