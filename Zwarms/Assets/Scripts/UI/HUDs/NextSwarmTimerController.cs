﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NextSwarmTimerController : MonoBehaviour {
    [SerializeField]
    private Text swarmText;
    [SerializeField]
    private Text timerDisplayer;

    #region MonoBehaviour functions
    private void Start() {
    }
    #endregion

    #region Public functions
    public void Deactivate () {
        swarmText.gameObject.SetActive(false);
        timerDisplayer.gameObject.SetActive(false);
    }

    public void Activate() {
        swarmText.gameObject.SetActive(true);
        timerDisplayer.gameObject.SetActive(true);
    }

    public void DisplayRemainingTime ( int remainingTimeInSeconds ) {
        timerDisplayer.text = remainingTimeInSeconds + "";
    }
    #endregion
}
