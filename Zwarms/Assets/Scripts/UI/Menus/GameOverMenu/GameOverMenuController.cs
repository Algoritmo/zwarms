﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverMenuController : MonoBehaviour {
    [SerializeField]
    private Text scoreDisplayer;
    [SerializeField]
    private GameObject newRecord;

    private int currentScore;

    #region MonoBehaviour functions
    private void Start() {
        currentScore = PlayerPrefs.GetInt(Constants.CURRENT_SCORE);

        scoreDisplayer.text = "Score: " + currentScore;

        EvaluateNewScore();
    }
    #endregion


    #region Private region
    private void EvaluateNewScore () {
        // saved scores
        /*
        for ( int i = 0; i < 10; i++) {
            Debug.Log("Score number " + i + "" + PlayerPrefs.GetInt( Constants.HIGHSCORE + i ));
        }
        */
        
        // if currentScore is greater than the last one, we have a new record
        if (currentScore > PlayerPrefs.GetInt(Constants.HIGHSCORE + (Constants.SCORES_AMOUNT - 1)) ) {
            newRecord.SetActive(true);

            // for each score excpet the last one, (because it was already compared)
            for ( int i = Constants.SCORES_AMOUNT - 2; i >= 0  ; i-- ) {

                // if currentScore is lesser than the current, saves it in the previous one
                if (  i != 0    &&  currentScore < PlayerPrefs.GetInt( Constants.HIGHSCORE + (i-1)) ) {
                    for (int j = Constants.SCORES_AMOUNT; j > i; j--) {
                        PlayerPrefs.SetInt( Constants.HIGHSCORE + j, PlayerPrefs.GetInt(Constants.HIGHSCORE + (j - 1)));
                    }

                    PlayerPrefs.SetInt( Constants.HIGHSCORE + (i), currentScore);
                    return;

                // if currentScore is greater than the greater highscore, overrides it
                } else if ( i == 0  &&  currentScore > PlayerPrefs.GetInt(Constants.HIGHSCORE + i)) {
                    for ( int j = Constants.SCORES_AMOUNT -1; j > 0; j-- ) {
                        PlayerPrefs.SetInt( Constants.HIGHSCORE + j,  PlayerPrefs.GetInt( Constants.HIGHSCORE + (j-1) ));
                    }

                    PlayerPrefs.SetInt( Constants.HIGHSCORE + i , currentScore);
                }
            }

        } else {
            newRecord.SetActive(false);
        }
    }
    #endregion


    #region Public functions
    public void PlayAgain() {
        SceneManager.LoadScene(Constants.MAIN_LEVEL);
    }

    public void MainMenu() {
        SceneManager.LoadScene(Constants.MAIN_MENU);
    }

    public void Quit () {
        Application.Quit();
    }
    #endregion
}
