﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class VolumeController : MonoBehaviour {
    [SerializeField]
    private AudioMixer audioMixer;
    [SerializeField]
    private Slider volumeSlider;

    private ConfigurationData configData;

    private void Start() {
        configData = new ConfigurationData();
        volumeSlider.value = configData.GetVolume();
        SetVolume(volumeSlider.value);
    }


    public void SetVolume ( float sliderValue ) {
        audioMixer.SetFloat("GlobalVolume", Mathf.Log10( sliderValue) * 20 );
        configData.SetVolume(sliderValue);
    }
}
