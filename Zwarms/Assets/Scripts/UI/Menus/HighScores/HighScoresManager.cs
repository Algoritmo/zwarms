﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScoresManager : MonoBehaviour {
    [SerializeField]
    private Text[] scoreDisplayers;


    #region MonoBehaviour functions
    private void Start() {
        DisplayScores();
    }
    #endregion


    #region Private functions
    private void DisplayScores() {
        for( int i = 0; i < scoreDisplayers.Length; i++ ) {
            scoreDisplayers[i].text = PlayerPrefs.GetInt(Constants.HIGHSCORE + i).ToString();
        }
    }
    #endregion


    #region Public functions
    public void ResetScores () {
        for ( int i = 0; i < Constants.SCORES_AMOUNT; i++ ) {
            PlayerPrefs.SetInt(Constants.HIGHSCORE + i, 0);
        }

        DisplayScores();
    }
    #endregion
}
