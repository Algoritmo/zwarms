﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonEventController : MonoBehaviour {
    private GameObject settingsMenu;

    #region MonoBehaviour functions
    private void Start() {
        settingsMenu = GameObject.Find("Settings");
        CloseSettingsMenu();
    }

    #endregion


    public void Resume () {
        GameController.instance.Resume();
    }

    public void GoToMainMenu () {
        SceneManager.LoadScene("MainMenu");
    }

    public void ShowSettingsMenu () {
        settingsMenu.SetActive(true);
    }

    public void CloseSettingsMenu () {
        settingsMenu.SetActive(false);
    }

    public void Quit () {
        Application.Quit();
    }
}
