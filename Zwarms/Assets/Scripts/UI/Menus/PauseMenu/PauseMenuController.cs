﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenuController : MonoBehaviour {
    private GameObject pauseMenuObject;

    private void Start() {
        pauseMenuObject = GameObject.Find("PauseMenu");
        CloseMenu();
    }


    #region Public methods
    public void OpenMenu () {
        pauseMenuObject.SetActive(true);
    }

    public void CloseMenu () {
        pauseMenuObject.SetActive(false);
    }
    #endregion
}
