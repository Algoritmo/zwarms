﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class MainMenuController : MonoBehaviour {
    private GameObject highscoresMenu;
    private GameObject settingsMenu;

    [SerializeField]
    private AudioClip menuBGM;
    [SerializeField]
    private AudioMixer globalAudioMixer;

    private AudioSource bgmAudioSource;



    #region MonoBehaviour functions
    private void Start() {
        highscoresMenu = GameObject.Find("Highscores");
        CloseHighscoresMenu();

        settingsMenu = GameObject.Find("Settings");
        CloseSettingsMenu();

        globalAudioMixer.SetFloat(Constants.GLOBAL_VOLUME, PlayerPrefs.GetFloat(Constants.GLOBAL_VOLUME, 1));
        bgmAudioSource = gameObject.AddComponent<AudioSource>();
        bgmAudioSource.outputAudioMixerGroup = globalAudioMixer.FindMatchingGroups("Master")[0];
        bgmAudioSource.loop = true;
        bgmAudioSource.clip = menuBGM;
        bgmAudioSource.Play();
    }
    #endregion


    public void PlayGame () {
        Debug.Log("cargando escena");
        Invoke("LoadGameLevelScene", 4);
        //SceneManager.LoadScene("TestLevel");
    }

    public void ShowHighscoresMenu() {
        highscoresMenu.SetActive(true);
    }

    public void CloseHighscoresMenu () {
        highscoresMenu.SetActive(false);
    }

    public void ShowSettingsMenu () {
        settingsMenu.SetActive(true);
    }

    public void CloseSettingsMenu () {
        PlayerPrefs.Save();
        settingsMenu.SetActive(false);
    }

    public void Quit () {
        Application.Quit();
    }

    #region Private functions
    private void LoadGameLevelScene() {
        SceneManager.LoadScene("TestLevel");
    }
    #endregion
}
