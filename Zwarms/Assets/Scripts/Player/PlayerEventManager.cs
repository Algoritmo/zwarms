﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEventManager : MonoBehaviour {
    private PlayerController playerController;

    private void Start() {
        playerController = gameObject.GetComponentInParent<PlayerController>();
    }

    private void AttackEvent () {
        playerController.AttackEvent();
    }

    private void PlayAttackingSFX() {
        playerController.PlayAttackingSFX();
    }
}
