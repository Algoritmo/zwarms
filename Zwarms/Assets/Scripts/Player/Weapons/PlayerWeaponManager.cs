﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeaponManager : MultipleWeaponManager {
    private static MultipleWeaponManager instance;
    public static MultipleWeaponManager Instance {
        get { return instance; }
    }

    private List<string> weaponsGameObjectsNames = new List<string>();
    private List<CustomSingleWeaponManager> weapons;

    public enum Weapons { BATE, REVOLVER, SHOTGUN };
    private CustomSingleWeaponManager currentWeapon;
    private CustomSingleWeaponManager previousWeapon;


    #region MonoBehaviour functions
    private void Awake() {
        // singleton
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        InitializeWeapons();
    }
    #endregion

    #region Initialization functions
    private void InitializeWeapons () {
        weapons = new List<CustomSingleWeaponManager>();

        InitializeWeaponsNames();
        InitializeSingleWeaponManagers();

        previousWeapon = GetWeapon(Weapons.BATE);
        currentWeapon = GetWeapon(Weapons.BATE);

        // need to use this function for the weapon to be visible
        //SwitchToWeapon(currentWeapon.weaponType);
    }

    private void InitializeWeaponsNames() {
        weaponsGameObjectsNames.Add("Bate");
        weaponsGameObjectsNames.Add("Revolver");
        weaponsGameObjectsNames.Add("Shotgun");
    }

    private void InitializeSingleWeaponManagers () {
        GameObject tmpGameObject;
        CustomSingleWeaponManager tempWeaponManager;

        foreach (string weaponName in weaponsGameObjectsNames) {
            tmpGameObject = GameObject.Find(weaponName);
            tempWeaponManager = tmpGameObject.GetComponent<CustomSingleWeaponManager>();
            weapons.Add( tempWeaponManager );
        }
    }
    #endregion

    #region Public functions
    public void SwitchToWeapon (PlayerWeaponManager.Weapons weapon ) {
        foreach ( CustomSingleWeaponManager weaponManager in weapons) {
            if (weaponManager.weaponType == weapon) {
                UpdateCurrentPreviousWeapons(weaponManager);
                UpdateAmmoHUD();
                return;
            }
        }
    }

    public override void SwitchToPreviousWeapon() {
        base.SwitchToPreviousWeapon();
        UpdateCurrentPreviousWeapons(previousWeapon);
        UpdateAmmoHUD();
    }

    public Weapons GetPreviousWeapon() {
        return previousWeapon.weaponType;
    }

    public int GetCurrentMagazineBulletsAmount () {
        return currentWeapon.weaponData.currentMagazineBullets;
    }

    public int GetNonMagazineBulletsAmount () {
        return currentWeapon.weaponData.nonMagazineBullets;
    }

    public int GetRemainingBullets () {
        return currentWeapon.weaponData.nonMagazineBullets;
    }

    public void DecreaseMagazineBulletsAmount () {
        if (currentWeapon.weaponData.currentMagazineBullets > 0) {
            currentWeapon.weaponData.currentMagazineBullets--;
            UpdateAmmoHUD();
        }
    }

    public int GetDamage() {
        return currentWeapon.weaponData.GetCalculatedDamage();
    }

    public float GetRange () {
        return currentWeapon.weaponData.Range;
    }

    public void Reload () {
        currentWeapon.CustomReload();
        UpdateAmmoHUD();
    }

    public void PlayAttackingSFX() {
        if (MayAttack())
            currentWeapon.PlayAttackingSound();
        else
            currentWeapon.PlayEmptyMagazineSound();
    }

    public bool MayAttack() {
        return currentWeapon.weaponData.MAX_MAGAZINE_CAPACITY == -1
            || currentWeapon.weaponData.currentMagazineBullets > 0;
    }

    public bool MayReload() {
        return currentWeapon.weaponData.currentMagazineBullets 
                            < currentWeapon.weaponData.MAX_MAGAZINE_CAPACITY
                    &&  currentWeapon.weaponData.nonMagazineBullets > 0;
    }

    public void IncreaseBulletsAmount ( int percentage ) {
        int tmpAmountToAdd;

        foreach( CustomSingleWeaponManager weapon in weapons ) {
            tmpAmountToAdd = (weapon.weaponData.MAX_TOTAL_BULLETS * percentage) / 100;
            weapon.weaponData.nonMagazineBullets += tmpAmountToAdd;
            if (weapon.weaponData.nonMagazineBullets + weapon.weaponData.currentMagazineBullets
                        > weapon.weaponData.MAX_TOTAL_BULLETS ) {
                weapon.weaponData.nonMagazineBullets =
                    weapon.weaponData.MAX_TOTAL_BULLETS - weapon.weaponData.MAX_MAGAZINE_CAPACITY;
            }
        }

        UpdateAmmoHUD();
    }
    #endregion

    #region Private methods
    private CustomSingleWeaponManager GetWeapon ( Weapons weapon ) {
        foreach( CustomSingleWeaponManager tempWeapon in weapons ) {
            if (tempWeapon.weaponType == weapon)
                return tempWeapon;
        }

        return null;
    }

    private void UpdateCurrentPreviousWeapons ( CustomSingleWeaponManager weaponManager ) {
        currentWeapon.gameObject.SetActive(false);

        previousWeapon = currentWeapon;
        currentWeapon = weaponManager;

        weaponManager.gameObject.SetActive(true);
        UpdateAmmoHUD();
    }

    private void UpdateAmmoHUD() {
        GameController.instance.UpdateAmmoDisplayer(
                currentWeapon.weaponData.currentMagazineBullets,
                currentWeapon.weaponData.nonMagazineBullets);
    }    
    #endregion
}
