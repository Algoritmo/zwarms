﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomSingleWeaponManager : SingleWeaponManager {
    public PlayerWeaponManager.Weapons weaponType;

    private WeaponSFXManager sfxManager;

    [SerializeField]
    private string weaponName;
    [SerializeField]
    private float range;
    [SerializeField]
    private int maxDamage;
    [SerializeField]
    private int magazineCapacity;
    [SerializeField]
    private int currentMagazineBulletsAmount;
    [SerializeField]
    private int nonMagazineBulletsAmount;
    [SerializeField]
    private int maxTotalBullets;

    #region MonoBehavior functions
    private void Awake() {
        weaponData = new WeaponData(
            weaponName,
            range,
            maxDamage,
            magazineCapacity,
            currentMagazineBulletsAmount, 
            nonMagazineBulletsAmount, 
            maxTotalBullets);

        sfxManager = gameObject.GetComponent<WeaponSFXManager>();
    }

    private void Start() {
        gameObject.SetActive(false);
    }
    #endregion

    #region Public functions
    public void CustomReload () {
        sfxManager.PlayReloadSound();
        Reload();
    }

    public void PlayAttackingSound() {
        sfxManager.PlayAttackSound();
    }

    public void PlayEmptyMagazineSound() {
        sfxManager.PlayEmptySound();
    }

    public void PlayHitSound() {
        sfxManager.PlayHitSound();
    }
    #endregion
}
