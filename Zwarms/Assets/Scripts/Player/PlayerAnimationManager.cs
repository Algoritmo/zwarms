﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationManager : AnimationManager {
    private Animator animator;

    private PlayerWeaponManager.Weapons lastWeapon = PlayerWeaponManager.Weapons.BATE;
    private PlayerWeaponManager.Weapons currentWeapon = PlayerWeaponManager.Weapons.BATE;
    private PlayerWeaponManager.Weapons tempWeapon;

    private bool isReloading = false;


    #region MonoBehaviour functions
    private void OnEnable() {
        if (!animator) {
            animator = GetComponentInChildren<Animator>();
        }
    }
    #endregion

    #region Public functions
    public void SwitchToMelee() {
        SwitchToASpecificWeapon(PlayerWeaponManager.Weapons.BATE);
    }

    public void SwitchToSecondary() {
        SwitchToASpecificWeapon(PlayerWeaponManager.Weapons.REVOLVER);
    }

    public void SwitchToHeavyGun() {
        SwitchToASpecificWeapon(PlayerWeaponManager.Weapons.SHOTGUN);
    }

    public void SwitchToPrevious () {
        SwitchToASpecificWeapon( lastWeapon );
    }

    public override void Attack() {
        animator.SetBool("isAttacking", true);
    }

    public override void Reload () {
        animator.SetBool("isReloading", true);
        Invoke("StopReloadingAnimation", .15f);
    }

     

    public void StopAttacking() {
        animator.SetBool("isAttacking", false);
    }    
    #endregion

    #region Private functions
    private void SwitchToASpecificWeapon(PlayerWeaponManager.Weapons weapon ) {
        StopAttacking();
        ResetHoldingBooleans();
        UpdatePreviousCurrentWeapons(weapon);

        switch (weapon) {
            case PlayerWeaponManager.Weapons.BATE:
                SwitchToBat();
                break;

            case PlayerWeaponManager.Weapons.REVOLVER:
                SwitchToRevolver();
                break;

            case PlayerWeaponManager.Weapons.SHOTGUN:
                SwitchToShotgun();
                break;

            default:
                print(weapon);
                break;
        }
    }

    private void SwitchToBat() {
        animator.SetBool("isHoldingBate", true);
    }

    private void SwitchToRevolver() {
        animator.SetBool("isHoldingRevolver", true);
    }

    private void SwitchToShotgun () {
        animator.SetBool("isHoldingShotgun", true);
    }

    private void ResetHoldingBooleans () {
        animator.SetBool("isHoldingRevolver", false);
        animator.SetBool("isHoldingBate", false);
        animator.SetBool("isHoldingShotgun", false);

    }

    private void UpdatePreviousCurrentWeapons(PlayerWeaponManager.Weapons newWeapon ) {
        tempWeapon = lastWeapon;
        lastWeapon = currentWeapon;
        currentWeapon = newWeapon;
    }

    private void StopReloadingAnimation() {
        animator.SetBool("isReloading", false);
    }
    #endregion
}
