﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameData {
    public enum STATES { RUNNING, PAUSED, OVER };

    private GameData.STATES _state;
    public GameData.STATES State {
        get { return _state; }
        set { _state = value; }
    }

    public GameData () {
        State = STATES.RUNNING;
    }


    public bool IsPaused () {
        return State == STATES.PAUSED;
    }

    public bool IsRunning () {
        return State == STATES.RUNNING;
    }

    public bool IsFinished () {
        return State == STATES.OVER;
    }

}
