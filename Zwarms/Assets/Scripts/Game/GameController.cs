﻿using UnityEngine;

public class GameController : MonoBehaviour {
    public static GameController instance;

    private GameData gameData;
    
    public bool EnemiesCanLive = true;

    #region Managers and Controllers
    private PlayerController playerController;

    private UIManager uiManager;
    private RoundManager roundManager;

    private GameObjectPoolManager dropPoolManager;
    private GameObjectPoolManager bloodSplashManager;
    #endregion


    #region MonoBehaviour
    private void Awake() {
        InitializeGameData();
        InitializeInstance();
        InitializeManagers();
    }

    private void Start() {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update() {
        CheckInput();
        
        if (gameData.State == GameData.STATES.RUNNING) {
            if (!roundManager.WaitingForNextRound()) {
                uiManager.UpdateTimer( roundManager.currentRoundTime );

            } else {
                BleedingScreenController.Instance.Deactivate();
                playerController.ProgressiveHealing();
                uiManager.UpdateLifeBar(((float) playerController.GetCurrentHealth()) / 100.0f);
            }
        }
    }
    #endregion



    #region Initialization functions
    private void InitializeGameData () {
        if (gameData == null)
            gameData = new GameData();
    }

    private void InitializeInstance() {
        //Singleton pattern
        if (instance == null) {
            instance = this;
        } else if (instance != this) {
            Destroy(gameObject);
        }
    }

    private void InitializeManagers() {
         roundManager = GameObject.Find("EnemySpawn").GetComponent<RoundManager>();
         uiManager = gameObject.AddComponent<UIManager>();
         dropPoolManager = GetComponent<GameObjectPoolManager>();
         playerController = GameObject.Find("FPS View").GetComponent<PlayerController>();
    }
    #endregion


    #region Input
    private void CheckInput() {
        if ( !gameData.IsPaused() ) {
            if ( Input.GetKeyDown( KeyCode.X)) {
                Pause();
            }
        } else {
            if (Input.GetKeyDown(KeyCode.X)) {
                Resume();
            }
        }
    }
    #endregion


    #region GameState functions
    public bool IsPaused () {
        return gameData.IsPaused();
    }

    public bool IsRunning () {
        return gameData.IsRunning();
    }
    #endregion



    #region HUD/UI functions
    public void UpdateAmmoDisplayer(int currentMagazineBulletsAmount, int remainingBullets) {
        uiManager.UpdateAmmunition(currentMagazineBulletsAmount, remainingBullets);
    }

    public void ShowDamage(Vector3 hudPosition, Quaternion hudRotation, int damage) {
        uiManager.AddDamageHud(hudPosition, hudRotation, damage);
    }

    public void ActivateNextSwarmTimer(int timeInSeconds ) {
        uiManager.ActivateNextSwarmTimer(timeInSeconds);
    }

    public void UpdateNextSwarmTimer( int timeInSeconds ) {
        uiManager.UpdateNextSwarmTimer(timeInSeconds);
    }

    public void DeactivateNextSwarmTimer() {
        uiManager.DeactivateNextSwarmTimer();
    }

    public void UpdateCurrentSwarmHUD ( int swarm ) {
        uiManager.UpdateSwarmHUD(swarm);
    }
    #endregion



    #region Public functions
    public void IncreaseScore ( int points ) {
        playerController.IncreaseScore(points);
        uiManager.UpdateHighScore(playerController.GetCurrentScore());
    }
    
    public void DealDamageToPlayer( int damage ) {
        playerController.ReceiveDamage(damage);
        uiManager.UpdateLifeBarWithDamage(((float) damage) / 100);
        CheckIfGameIsOver();
    }
     
    public void SpawnNewDropBox( Vector3 dropPosition) {
        dropPoolManager.UseInstance(dropPosition);
    }

    public void SpawnNewBloodSplash ( Vector3 hitPosition ) {
        VFXManager.Instance.SpawnBloodSplash( hitPosition );
    }

    public void DecrementCurrentEnemiesAmount () {
        roundManager.DecrementEnemiesAmount();
    }

    public void IncreaseAmmo (int percentageAmount) {
        playerController.IncreaseBulletsAmount(percentageAmount);
    }
   
    public void Resume() {
        playerController.UpdateKeys();

        Time.timeScale = 1;
        gameData.State = GameData.STATES.RUNNING;
        uiManager.ClosePauseMenu();
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        BGMManager.Instance.Resume();
    }

     public Vector3 GetPlayerPosition() {
        return playerController.GetPosition();
    }
    
    public int GetCurrentEnemiesAmount() {
        return 0;
    }
    
    public void FinishRound () {
        dropPoolManager.HideAll();
    }
    #endregion

    #region Private functions
    private void CheckIfGameIsOver() {
        if ( gameData.IsRunning()   &&    playerController.GetCurrentHealth() <= 0) {
            FinishGame();
        } else {
            if (playerController.HasLowHP()) {
                BleedingScreenController.Instance.Activate();
            } else {
                BleedingScreenController.Instance.Deactivate();
            }
        }
    }

    private void Pause () {
        Time.timeScale = 0;
        gameData.State = GameData.STATES.PAUSED;
        uiManager.OpenPauseMenu();
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

        BGMManager.Instance.Pause();
    }

    private void FinishGame() {
        Debug.Log("FinishGame - increasing score");
        gameData.State = GameData.STATES.OVER;
        IncreaseScore(roundManager.GetCurrentRoundTimeScore());
        PlayerPrefs.SetInt(Constants.CURRENT_SCORE, playerController.GetCurrentScore());
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        GameOverTransitionController.Instance.StartTransition();
    }
    #endregion
}
