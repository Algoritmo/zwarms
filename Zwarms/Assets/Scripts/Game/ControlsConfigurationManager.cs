﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlsConfigurationManager : MonoBehaviour {
    private ConfigurationData configData;

    [SerializeField]
    private Text forward;
    [SerializeField]
    private Text backward;
    [SerializeField]
    private Text left;
    [SerializeField]
    private Text right;
    [SerializeField]
    private Text reload;
    [SerializeField]
    private Text previous;
    [SerializeField]
    private Text jump;
    [SerializeField]
    private Text fire;


    private GameObject currentKey;




    #region MonoBehaviour functions
    private void Start() {
        configData = new ConfigurationData();

        RefreshConfigurationDisplayer();
    }

    private void OnGUI() {
        if ( currentKey != null ) {
            Event e = Event.current;

            if ( e.isKey ) {
                configData.SetNewKey(currentKey.name, e.keyCode);
                currentKey.transform.GetChild(0).GetComponent<Text>().text = e.keyCode.ToString();
                currentKey = null;
            }
        }
    }
    #endregion

    #region Public functions
    public void ChangeKey ( GameObject clicked ) {
        currentKey = clicked;
    }

    public void ResetConfiguration () {
        configData.Reset();
        RefreshConfigurationDisplayer();
    }
    #endregion

    #region Private functions
    private void RefreshConfigurationDisplayer () {
        forward.text = configData.GetKeyString(Constants.FORWARD);
        backward.text = configData.GetKeyString(Constants.BACKWARD);
        left.text = configData.GetKeyString(Constants.LEFT);
        right.text = configData.GetKeyString(Constants.RIGHT);

        previous.text = configData.GetKeyString(Constants.PREVIOUS_WEAPON);
        jump.text = configData.GetKeyString(Constants.JUMP);
        reload.text = configData.GetKeyString(Constants.RELOAD);
        fire.text = configData.GetKeyString(Constants.FIRE_MOUSE);
    }
    #endregion
}
