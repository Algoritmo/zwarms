﻿using UnityEngine;

public class SingleEnemySFXManager : MonoBehaviour {
    private AudioSource standingBy;
    private AudioSource attacking;
    private AudioSource dying;

    private float defaultVolume = .8f;
    private float spatialBlend = 1f;
    private float maxDistance = 7f;

    public bool isInitialized = false;

    #region MonoBehaviour functions
    private void Awake() {
        InitializeAudioSources();
    }

    #endregion


    #region Public functions
    public void PlayStandingBySound() {
        PlayAudioSource(standingBy);
    }

    public void PlayAttackingSound() {
        PlayAudioSource(attacking);
    }

    public void PlayDyingBySound() {
        PlayAudioSource(dying);
    }
    #endregion


    #region Private functions
    private void PlayAudioSource( AudioSource audioSourceToPlay) {
        if ( audioSourceToPlay != null && !audioSourceToPlay.isPlaying ) {
            StopAllAudioSources();
            audioSourceToPlay.Play();

        } else if ( audioSourceToPlay == null ) {
            InitializeAudioSources();
        }
    }

    private void StopAllAudioSources () {
        standingBy.Stop();
        attacking.Stop();
        dying.Stop();
    }

    private void InitializeAudioSources() {
        if (EnemiesSFXManager.Instance != null && EnemiesSFXManager.Instance.isInitialized) {
            standingBy = gameObject.AddComponent<AudioSource>();
            SetUpAudioSource(standingBy);
            standingBy.clip = EnemiesSFXManager.Instance.GetStandingClip();

            attacking = gameObject.AddComponent<AudioSource>();
            SetUpAudioSource(attacking);
            attacking.clip = EnemiesSFXManager.Instance.GetAttackingClip();

            dying = gameObject.AddComponent<AudioSource>();
            SetUpAudioSource(dying);
            dying.clip = EnemiesSFXManager.Instance.GetDyingClip();

            isInitialized = true;
        }
    }

    private void SetUpAudioSource( AudioSource audioSourceToSetUp ) {
        audioSourceToSetUp.spatialBlend = spatialBlend;
        audioSourceToSetUp.volume = defaultVolume;
        audioSourceToSetUp.maxDistance = maxDistance;
    }
    #endregion
}
