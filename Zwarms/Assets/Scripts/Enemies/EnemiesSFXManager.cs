﻿using UnityEngine;
using UnityEngine.Audio;

public class EnemiesSFXManager : MonoBehaviour {
    private static EnemiesSFXManager instance;
    public static EnemiesSFXManager Instance {
        get { return instance; }
    }

    [SerializeField]
    private AudioClip standingByClip;
    [SerializeField]
    private AudioClip attackingClip;
    [SerializeField]
    private AudioClip dyingClip;

    [SerializeField]
    private AudioMixer audioMixer;

    public bool isInitialized = false;

    #region MonoBehaviour functions
    private void Awake() {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    private void Start() {
        isInitialized = true;
    }
    #endregion

    #region Public functions
    public AudioClip GetStandingClip () {
        return standingByClip;
    }

    public AudioClip GetAttackingClip() {
        return attackingClip;
    }

    public AudioClip GetDyingClip() {
        return dyingClip;
    }
    #endregion
}
