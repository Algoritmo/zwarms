﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieAnimationManager : AnimationManager {
    private Animator animator;

    private void Awake() {
        animator = GetComponent<Animator>();
    }

    public override void StandBy () {
        base.StandBy();

        ResetBooleans();
        animator.SetBool("isIdle", true);
    }

    public override void Walk() {
        base.Walk();

        ResetBooleans();
        animator.SetBool("isRunning", true);
    }

    public override void Run() {
        base.Run();

        ResetBooleans();
        animator.SetBool("isRunning", true);
    }

    public override void Attack() {
        base.Attack();

        ResetBooleans();
        animator.SetBool("isAttacking", true);
    }

    public override void Death() {
        base.Death();
        ResetBooleans();
        animator.SetBool("isDead", true);
    }


    public override bool AnimationHasFinished() {
        return animator.GetCurrentAnimatorStateInfo(0).normalizedTime > .5f;
    }
    
    public override bool AnimationHasJustStarted() {
        return animator.GetCurrentAnimatorStateInfo(0).normalizedTime <= .5f;

    }

    public override void ResetAnimator() {
        ResetBooleans();
        animator.SetBool("isIdle", true);
    }



    private void ResetBooleans () {
        animator.SetBool("isDead", false);
        animator.SetBool("isRunning", false);
        animator.SetBool("isAttacking", false);
    }

}
