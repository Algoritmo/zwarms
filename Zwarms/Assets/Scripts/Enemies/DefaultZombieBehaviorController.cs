﻿using UnityEngine;
using UnityEngine.AI;

public enum EnemyState {
    IDLE,
    WALK,
    RUN,
    PAUSE,
    GOBACK,
    ATTACK,
    DEATH
}

public class DefaultZombieBehaviorController : CustomMonoBehaviour {
    private PlayerData zombieData;
    private ZombieAnimationManager animationManager;
    private SingleEnemySFXManager sfxManager;

    private bool isDead = false;

    [SerializeField]
    private float attackDistance = 2f;
    [SerializeField]
    private float alertAttackDistance = 200f;
    [SerializeField]
    private float followDistance = 500f;

    private int damage = 20;


    [HideInInspector]
    public EnemyState enemyCurrentState = EnemyState.IDLE;
    private EnemyState enemyLastState = EnemyState.IDLE;

    private Animator anim;

    private NavMeshAgent navAgent;
    private Vector3 initialPosition;
    private Vector3 whereToNavigate;

    private CharacterController charController;
    private Vector3 whereToMove = Vector3.zero;

    private static float WAIT_TIME = 3f;
    private float waitingTimeLeft = WAIT_TIME;
    

    #region MonoBehaviour functions
    private void Awake() {
        charController = GetComponent<CharacterController>();
        sfxManager = gameObject.AddComponent<SingleEnemySFXManager>();
        animationManager = gameObject.AddComponent<ZombieAnimationManager>();
        zombieData = new PlayerData(100, 5f, 8f, 6.6f, 20f);
    }

    void Start() {
        navAgent = GetComponent<NavMeshAgent>();
        charController = GetComponent<CharacterController>();
        anim = GetComponent<Animator>();

        zombieData.score = 5;

        initialPosition = transform.position;
        whereToNavigate = transform.position;
    }

    private void OnEnable() {
        Reset();
    }
    
    void Update() {
        CheckState();
    }
    #endregion
    

    #region Public functions
    public void ReceiveDamage( int damage) {
        zombieData.DealDamage(damage);
        
        if ( !zombieData.IsAlive() && !isDead ) {
            GameController.instance.IncreaseScore(zombieData.score);
            Die();
        }
    }


    public void Die() {
        GameController.instance.DecrementCurrentEnemiesAmount();
        enemyCurrentState = EnemyState.DEATH;
        isDead = true;
        animationManager.Death();
        zombieData.Reset();
        sfxManager.PlayDyingBySound();
    }

    public bool IsDead () {
        return isDead;
    }
    #endregion


    #region Private functions
    private void CheckState() {
        if (GameController.instance.EnemiesCanLive) {
            if (enemyCurrentState != EnemyState.DEATH) {
                CheckAttackRange();

            } else {
                CheckZombieDeactivationDelay();
            }

        } else {
            Die();
            CheckZombieDeactivationDelay();
        }
    }


    private void CheckAttackRange() {
        if ( PlayerIsInAttackRange() ) {
            SetToAttackState();

        } else if ( PlayerIsInFollowRange() ) {
            sfxManager.PlayStandingBySound();
            SetToFollowState();

        } else {
            sfxManager.PlayStandingBySound();
            SetToIdleState();
        }

        navAgent.SetDestination(whereToMove);
    }


    private bool PlayerIsInAttackRange () {
        return Vector3.Distance(GameController.instance.GetPlayerPosition(), transform.position) < attackDistance;
    }

    private bool PlayerIsInFollowRange() {
        return Vector3.Distance(GameController.instance.GetPlayerPosition(), transform.position) < followDistance
                && Vector3.Distance(GameController.instance.GetPlayerPosition(), transform.position) > attackDistance;
    }



    private void SetToAttackState() {
        if (enemyCurrentState != EnemyState.ATTACK) {
            enemyLastState = enemyCurrentState;
            enemyCurrentState = EnemyState.ATTACK;
            animationManager.Attack();
        }

        whereToMove = transform.position;
    }


    private void SetToFollowState() {
        if (enemyCurrentState != EnemyState.RUN) {
            enemyLastState = enemyCurrentState;
            enemyCurrentState = EnemyState.RUN;
            animationManager.Run();
        }

        whereToMove = GameController.instance.GetPlayerPosition();
    }


    private void SetToIdleState () {
        if (enemyCurrentState != EnemyState.IDLE) {
            enemyLastState = enemyCurrentState;
            enemyCurrentState = EnemyState.IDLE;
            animationManager.StandBy();
        }

        whereToMove = transform.position;
    }

    

    public override void Reset() {
        gameObject.tag = tag;
        charController.enabled = true;
        isDead = false;
        enemyCurrentState = EnemyState.IDLE;
        waitingTimeLeft = WAIT_TIME;

        zombieData.Reset();
        animationManager.ResetAnimator();
    }

    private void CheckZombieDeactivationDelay() {
        navAgent.SetDestination(transform.position);
        waitingTimeLeft -= Time.deltaTime;
        if (waitingTimeLeft < 0) {
            gameObject.SetActive(false);
            GenerateDrop();
        }
    }

    private void GenerateDrop () {
        if (Random.value <= 1f)
            GameController.instance.SpawnNewDropBox(transform.position);
    }
    #endregion



    #region Event functions
    private void DealDamageToPlayerEvent() {
        if ( PlayerIsInAttackRange() )
            GameController.instance.DealDamageToPlayer(damage);
    }
    #endregion
}
